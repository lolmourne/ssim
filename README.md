How to run this project
1. install opencv library, please refer to this page https://docs.opencv.org/3.4/d7/d9f/tutorial_linux_install.html
2. cd to directory and execute `make` to build SSIM application (if needed)
3. run this command to compare two images `./SSIM img/test.jpg img/test2.png`